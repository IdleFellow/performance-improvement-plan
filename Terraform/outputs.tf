output "appserver_dns_name" {
  value = module.appserver.ec2_instance_values.public_dns
}
