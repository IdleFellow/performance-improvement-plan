variable "aws_region" {
  type    = string
  default = "eu-west-1"
}

variable "application" {
  type    = map(string)
  default = {
    "description" = "TODO Application"
    "name"        = "TodoApp"
    "playbook"    = "install_app.yml"
  }
}

variable "ports" {
  type    = list
  default = [ 8080, 9411, 18080, 19411 ]
}
