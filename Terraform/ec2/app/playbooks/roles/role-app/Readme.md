App Role
=========

Install App

Role Description
------------

This role installs and configures the target application


Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

- name: install App
  hosts: working_group
  user: ec2-user
  become_method: sudo
  become: yes
  roles:
    - { role: role-app }

Author Information
------------------

<Andrea De Rinaldis> andrea.derinaldis@hcl.com
