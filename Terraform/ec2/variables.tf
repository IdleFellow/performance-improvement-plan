variable "aws_region" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "private_subnets" {
  default = []
}

variable "public_subnets" {
  default = []
}

variable "instance_ami" {
  type        = string
  default     = "ami-0f89681a05a3a9de7" # AMI Linux
  # default     = "ami-0943382e114f188e8" # Ubuntu
  # default     = "ami-0874dad5025ca362c" # Debian
  # default     = "ami-0ec23856b3bad62d3" # RHEL 8
}

variable "instance_type" {
  type        = string
  default     = "t2.small"
}

variable "key_pair" {
  type        = string
  default     = "kirey-key"
}

variable "ec2_user" {
  type        = string
  default     = "ec2-user"
  # default     = "ubuntu"
  # default     = "admin"
}

variable "root_device_type" {
  type        = string
  default     = "gp2"
}

variable "root_device_size" {
  type        = string
  default     = "20"
}

variable "application" {
  default = {}
}

variable ports {
  default = []
}
