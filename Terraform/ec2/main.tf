resource "aws_instance" "instance" {
  ami                         = var.instance_ami
  availability_zone           = data.aws_availability_zones.available.names[0]
  instance_type               = var.instance_type
  associate_public_ip_address = true
  vpc_security_group_ids      = [ aws_security_group.security_group.id ]
  subnet_id                   = "${var.public_subnets[0]}"
  key_name                    = var.key_pair

  root_block_device {
    delete_on_termination = true
    encrypted             = false
    volume_size           = var.root_device_size
    volume_type           = var.root_device_type
  }

  tags = {
    "Name"                = var.application.name
  }

  provisioner "remote-exec" {
      connection {
        type        = "ssh"
        user        = "${var.ec2_user}"
        host        = "${self.public_dns}"
        private_key = file("${path.module}/keys/hcl-key.pem")
      }

      inline = [
        "sudo yum -y update",
        "sudo amazon-linux-extras install -y ansible2 docker",
    ]
  }

  provisioner "local-exec" {
    command     = "ansible-playbook --private-key ../keys/hcl-key.pem -e working_host=$TARGET playbooks/${var.application.playbook}"
    working_dir = "${path.module}/app"
    environment = {
      TARGET = "${self.public_dns}"
    }
  }
}
