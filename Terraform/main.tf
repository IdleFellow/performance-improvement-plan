module "network" {
  source = "./network"

  aws_region = "${var.aws_region}"
}

module "dns" {
  source = "./dns"

  aws_region  = "${var.aws_region}"
  vpc_id      = module.network.vpc_id
  application = "${var.application}"
}

module "appserver" {
  source = "./ec2"

  aws_region = "${var.aws_region}"
  vpc_id = module.network.vpc_id
  private_subnets = module.network.private_subnets
  public_subnets = module.network.public_subnets
  application = "${var.application}"
  ports = "${var.ports}"
}

# module "buildserver" {
#   source = "./ec2"
# 
#   aws_region = "${var.aws_region}"
#   vpc_id = module.network.vpc_id
#   private_subnets = module.network.private_subnets
#   public_subnets = module.network.public_subnets
#   application = "${var.jenkins_application}"
#   ports = "${var.jenkins_ports}"
# }

resource "local_file" "ec2_instance" {
  content     = module.appserver.ec2_instance_values.public_dns
  filename    = "${path.module}/ec2.txt"
}
