output "vpc_id" {
  value       = "${module.vpc.vpc_id}"
  description = "The VPC ID."
}

output "private_subnets" {
  value       = "${module.vpc.private_subnets}"
  description = "The Private Subnets."
}

output "public_subnets" {
  value       = "${module.vpc.public_subnets}"
  description = "The Public Subnets."
}
