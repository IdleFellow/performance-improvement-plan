resource "aws_service_discovery_private_dns_namespace" "internal" {
  name        = "${var.application.name}"
  description = "Application internal zone"
  vpc         = "${var.vpc_id}"
}
