variable "aws_region" {
  default = ""
}

variable "vpc_id" {
  default = ""
}

variable "application" {
  default = {}
}
