output internal_namespace_values {
  value       = aws_service_discovery_private_dns_namespace.internal
  description = "The details of the Internal Namespace."
}
